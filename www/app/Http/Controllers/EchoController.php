<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


class EchoController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {

        $items = $request->input('items');

        $return = [
            'status' => 200,
            'payload' => $items,
            'total' => count($items)
        ];

        return response()->json($return);
    }
}