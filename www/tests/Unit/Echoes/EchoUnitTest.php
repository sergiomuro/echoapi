<?php

namespace Tests\Unit\Echoes;

use App\Http\Controllers\EchoController;
use Illuminate\Http\Request;
use Tests\TestCase;
use Faker;

class EchoUnitTest extends TestCase
{
    private $faker;

    /** @test */
    public function testIndex()
    {
        $this->faker = Faker\Factory::create();
        $data = [
            'title' => $this->faker->text,
            'link' => $this->faker->text,
            'src' => $this->faker->text,
        ];

        $request = new \Illuminate\Http\Request();
        $request->replace($data);

        $echoController = new EchoController();
        $echo = $echoController->index($request);

        $this->assertJson(json_encode($data), $echo);
    }
}