<?php

namespace Tests\Unit\Auth;

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Tests\TestCase;
use Faker;

class AuthUnitTest extends TestCase
{
    private $faker;
    private $user;

    /** @test */
    public function testSignup()
    {
        $this->faker = Faker\Factory::create();
        $userName = $this->faker->name;
        $userEmail = $this->faker->email;
        $userPass = $this->faker->password;

        $this->user = [
            'name' => $userName,
            'email' => $userEmail,
            'password' => $userPass,
            'password_confirmation' => $userPass,
        ];

        $request = new Request();
        $request->replace($this->user);

        $authController = new AuthController();
        $signup = $authController->signup($request);

        $this->assertJson(json_encode(['message' => 'Successfully created user!']), $signup);
    }
}
