# Echo Project
#### Requirements

* [Git](https://git-scm.com/downloads)
* [Docker](https://www.docker.com/products/docker-engine) >= 17.12
* [Docker Compose](https://docs.docker.com/compose/install/)

Clone this repository anywhere on your machine:
```bash
git clone https://gitlab.com/sergiomuro/echoapi.git
```
Into your project folder  
Go to laradock folder and create .env file
```bash
cd laradock
cp env-example .env
```
 Change the nginx port into the .env file if you need this (line 199)  
 *(into laradock folder)*  
```bash
NGINX_HOST_HTTP_PORT=80
```

### Run the project
Into your laradock project folder  
Run daemon containers *(into laradock folder)*
```bash
sudo docker-compose up -d nginx mysql workspace
```

### Required installations
Enter into the project bash *(into laradock folder)*
```bash
sudo docker-compose exec workspace bash
```
Alternatively, for Windows PowerShell users: execute the following command to enter any running container:

```bash
docker exec -it {workspace-container-id} bash
```

#### Required step
```bash
composer install
php artisan migrate
php artisan passport:install
```

## Test the project
Open your browser and visit your localhost address [http://localhost/](http://localhost/)
to check if the server is running

## Use PostMan
You can see the project API resources [here](https://documenter.getpostman.com/view/2038371/S1EWNuPv)
